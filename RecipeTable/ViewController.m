//
//  ViewController.m
//  RecipeTable
//
//  Created by Jordan Hipwell on 1/26/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import "ViewController.h"
#import "RecipeInfoViewController.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSArray *recipeData;
@property (strong, nonatomic) UITableView *tableView;

@end


@implementation ViewController

static NSString * RecipeDataID = @"recipeData";
static NSString * RecipeTitleID = @"recipeTitle";
static NSString * RecipeImageID = @"recipeImage";
static NSString * RecipeInstructionsID = @"recipeInstructions";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    //get recipe data from plist
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Property List" ofType:@"plist"];
    self.recipeData = [[NSArray alloc] initWithContentsOfFile:path];
    
    //if no recipe data (corrupt plist possibly?), get from NSUserDefaults, otherwise save to defaults
    if (self.recipeData.count > 0) {
        [userDefaults setObject:self.recipeData forKey:RecipeDataID];
        [userDefaults synchronize];
    } else {
        self.recipeData = [userDefaults objectForKey:RecipeDataID];
    }
    
    //set up background
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    backgroundImageView.image = [UIImage imageNamed:@"wood-bg"];
    backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:backgroundImageView];
    
    //set up table view
    CGFloat statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
    CGFloat verticalPosition = statusBarHeight + self.navigationController.navigationBar.frame.size.height;
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, verticalPosition, self.view.frame.size.width, self.view.frame.size.height - verticalPosition)];
    tableView.backgroundColor = [UIColor clearColor];
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    tableView.separatorEffect = [UIVibrancyEffect effectForBlurEffect:blurEffect];
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.rowHeight = 100;
    [self.view addSubview:tableView];
    self.tableView = tableView;
    
    //make nav bar transparent
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    
    //custom back button
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                 style:UIBarButtonItemStylePlain
                                                                target:nil
                                                                action:nil];
    [self.navigationItem setBackBarButtonItem:backItem];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //clear cell selection interactively upon swipe back
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
    
    self.title = @"Recipes";
    UIFont *titleFont = [UIFont fontWithName:@"Zapfino" size:18];
    UIColor *titleColor = [[UIColor blackColor] colorWithAlphaComponent:0.65];
    NSDictionary *titleAttributes = @{ NSFontAttributeName: titleFont,
                                       NSForegroundColorAttributeName: titleColor };
    self.navigationController.navigationBar.titleTextAttributes = titleAttributes;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.recipeData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) { //haven’t created a cell with that id before
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    //selected background view
    UIView *selectedView = [[UIView alloc] init];
    selectedView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    selectedView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.4];
    cell.selectedBackgroundView = selectedView;
    
    //recipe image
    CGFloat padding = 8;
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(tableView.separatorInset.left, padding, 100 - padding, tableView.rowHeight - padding * 2)];
    imageView.image = [UIImage imageNamed:self.recipeData[indexPath.row][RecipeImageID]];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    [cell.contentView addSubview:imageView];
    
    //recipe title
    CGFloat horizontalPosition = imageView.frame.origin.x + imageView.frame.size.width + 15;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(horizontalPosition, 0, cell.frame.size.width - horizontalPosition, tableView.rowHeight)];
    label.text = self.recipeData[indexPath.row][RecipeTitleID];
    label.numberOfLines = 2;
    label.font = [UIFont fontWithName:@"Papyrus" size:[UIFont preferredFontForTextStyle:UIFontTextStyleBody].pointSize];
    label.textColor = [UIColor blackColor];
    [cell.contentView addSubview:label];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    RecipeInfoViewController *recipeInfoVC = [[RecipeInfoViewController alloc] init];
    recipeInfoVC.recipeData = self.recipeData[indexPath.row];
    
    [self.navigationController pushViewController:recipeInfoVC animated:YES];
}


@end
