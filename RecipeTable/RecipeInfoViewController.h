//
//  RecipeInfoViewController.h
//  RecipeTable
//
//  Created by Jordan Hipwell on 1/28/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecipeInfoViewController : UIViewController

@property (nonatomic, strong) NSDictionary *recipeData;

@end
