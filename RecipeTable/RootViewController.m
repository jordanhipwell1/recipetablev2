//
//  RootViewController.m
//  RecipeTable
//
//  Created by Jordan Hipwell on 1/28/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import "RootViewController.h"
#import "ViewController.h"

@interface RootViewController ()

@end

@implementation RootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    ViewController *vc = [[ViewController alloc] init];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:vc];
    [self addChildViewController:navController];
    [self.view addSubview:navController.view];
}

@end
